//
//  EnterRouteViewControllerTests.swift
//  GOGOXDemoTests
//
//  Created by Alex Ng on 16/4/2021.
//

import XCTest

class EnterRouteViewControllerTests: XCTestCase {

    var sut: EnterRouteViewController!
    var window: UIWindow = UIWindow()

    var viewModel: EnterRouteViewModelSpy!
    var router: EnterRouteViewRouterSpy!

    override func setUp() {
        super.setUp()

        sut = EnterRouteViewController.initFromXIB()
        window.addSubview(sut.view)

        viewModel = EnterRouteViewModelSpy()
        router = EnterRouteViewRouterSpy()

        sut.viewModel = viewModel
        sut.router = router

        viewModel.vc = sut
        router.vc = sut
    }

    func test_Click_backButtoc() {
        let vc = OnboardingViewController.initFromXIB()
        window.addSubview(vc.view)

        let vcViewModel = OnboardingViewControllerTests.OnboardingViewModelSpy()
        let vcRouter = OnboardingViewControllerTests.OnboardingViewRouterSpy()

        vc.viewModel = vcViewModel
        vc.router = vcRouter

        vcViewModel.vc = vc
        vcRouter.vc = vc

        let g1 = GGXPlace(name: "Hong Kong", gPlaceId: "")
        let g2 = GGXPlace(name: "Kwun Tong", gPlaceId: "")

        viewModel.model.origin = g1
        viewModel.model.destination = g2

        sut.delegate = vc
        sut.backButtonClicked(UIButton())

        XCTAssertTrue(router.dismissCalled)
        XCTAssertTrue(vcViewModel.toUpdateRoutePlaceCalled)
        XCTAssertEqual(vcViewModel.model.origin?.name, g1.name)
        XCTAssertEqual(vcViewModel.model.destination?.name, g2.name)
    }

    func test_Get_AutoCompleSuccess() {
        viewModel.service = GooglePlaceServiceSuccessSpy()
        sut.routeChanged(place: "Test", input: .origin)

        XCTAssertEqual(viewModel.currentInput, .origin)
        XCTAssertTrue(viewModel.toGetAutocompleteCalled)
        XCTAssertNotNil(viewModel.model.searchResult?.count)
    }
}

// MARK: ViewModel Spy
extension EnterRouteViewControllerTests {

    class EnterRouteViewModelSpy: EnterRouteViewModel {

        var toGetAutocompleteCalled  = false
        override func toGetAutocomplete(keyword: String) {
            super.toGetAutocomplete(keyword: keyword)

            toGetAutocompleteCalled = true
        }
    }
}

//MARK: Router Spy
extension EnterRouteViewControllerTests {

    class EnterRouteViewRouterSpy: EnterRouteViewRouter {

        var dismissCalled = false
        override func dismiss() {
            super.dismiss()

            dismissCalled = true
        }
    }
}
