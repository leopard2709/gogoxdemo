//
//  OnboardingViewControllerTests.swift
//  GOGOXDemoTests
//
//  Created by Alex Ng on 16/4/2021.
//

import XCTest

class OnboardingViewControllerTests: XCTestCase {

    var sut: OnboardingViewController!
    var window: UIWindow = UIWindow()

    var viewModel: OnboardingViewModelSpy!
    var router: OnboardingViewRouterSpy!


    override func setUp() {
        super.setUp()

        sut = OnboardingViewController.initFromXIB()
        window.addSubview(sut.view)

        viewModel = OnboardingViewModelSpy()
        router = OnboardingViewRouterSpy()

        sut.viewModel = viewModel
        sut.router = router

        viewModel.vc = sut
        router.vc = sut
    }

    func test_Click_RouteBar() {
        sut.routeBarClicked()

        XCTAssertTrue(router.routeToEnterRouteCalled)
    }
}

// MARK: ViewModel Spy
extension OnboardingViewControllerTests {

    class OnboardingViewModelSpy: OnboardingViewModel {

        var toUpdateRoutePlaceCalled = false
        override func toUpdateRoutePlace(origin: GGXPlace?, destination: GGXPlace?) {
            super.toUpdateRoutePlace(origin: origin, destination: destination)

            toUpdateRoutePlaceCalled = true
        }
    }
}

//MARK: Router Spy
extension OnboardingViewControllerTests {

    class OnboardingViewRouterSpy: OnboardingViewRouter {

        var routeToEnterRouteCalled = false
        override func routeToEnterRoute() {
            super.routeToEnterRoute()

            routeToEnterRouteCalled = true
        }
    }
}
