//
//  GooglePlaceServiceTests.swift
//  GOGOXDemoTests
//
//  Created by Alex Ng on 16/4/2021.
//

import XCTest

class GooglePlaceServiceSuccessSpy: GooglePlaceService {
    

    override func getAutocomplete(keyword: String, completion: @escaping (Result<[GGXPlace]?, Error>) -> Void) {

        let g1 = GGXPlace(name: "Hong Kong", gPlaceId: "")
        let g2 = GGXPlace(name: "Kwun Tong", gPlaceId: "")
        let g3 = GGXPlace(name: "Causeway Bay", gPlaceId: "")

        completion(.success([g1, g2, g3]))
    }
}

class GooglePlaceServiceErrorSpy: GooglePlaceService {

    override func getAutocomplete(keyword: String, completion: @escaping (Result<[GGXPlace]?, Error>) -> Void) {
        let nsError = NSError(domain: "", code: 0000, userInfo: nil)
        completion(.failure(nsError))
    }
}
