//
//  GooglePlaceService.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import Foundation
import GooglePlaces

class GooglePlaceService {

    let token = GMSAutocompleteSessionToken()

    func getAutocomplete(keyword: String, completion: @escaping (Result<[GGXPlace]?, Error>) -> Void) {
        let client = GMSPlacesClient.shared()
        client.findAutocompletePredictions(fromQuery: keyword,
                                           filter: nil,
                                           sessionToken: token) { (result, error) in
            if let error = error {
                completion(.failure(error))
                return
            }

            let list = result?.compactMap { return GGXPlace(name: $0.attributedFullText.string, gPlaceId: $0.placeID) }

            completion(.success(list))
        }
    }
}


