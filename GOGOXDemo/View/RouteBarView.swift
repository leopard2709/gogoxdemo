//
//  RouteBarView.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 15/4/2021.
//

import UIKit

protocol RouteBarViewDelegate: class {
    func routeChanged(place: String, input: RouteBarView.RouteInput)
}

class RouteBarView: UIView {

    enum RouteInput {
        case origin
        case destination
    }

    @IBOutlet var view: UIView!
    @IBOutlet weak var originTextField: GGXTextField!
    @IBOutlet weak var destinationTextField: GGXTextField!

    public weak var delegate: RouteBarViewDelegate?

    public var isDisplayMode: Bool = false {
        didSet {
            originTextField.isEnabled = !isDisplayMode
            destinationTextField.isEnabled = !isDisplayMode
        }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        Bundle.main.loadNibNamed("RouteBarView", owner: self, options: nil)
        view.frame = self.bounds
        addSubview(view)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        initUI()
        setupTextField()
    }

    private func initUI() {
        originTextField.placeholder = "Select pick-up point"
        destinationTextField.placeholder = "Select drop-off point"
    }

    private func setupTextField() {
        originTextField.delegate = self
        destinationTextField.delegate = self
    }
}

extension RouteBarView: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let text = textField.text as NSString?
        let place = text?.replacingCharacters(in: range, with: string) ?? ""

        switch textField {
        case originTextField:
            delegate?.routeChanged(place: place, input: .origin)
        case destinationTextField:
            delegate?.routeChanged(place: place, input: .destination)
        default:
            break
        }

        return true
    }
}
