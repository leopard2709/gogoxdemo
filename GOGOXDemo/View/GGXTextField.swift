//
//  GGXTextField.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 15/4/2021.
//

import UIKit

class GGXTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()

        borderStyle = .roundedRect
    }

}
