//
//  ViewController.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 15/4/2021.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view

    }

    @IBAction func demoButtonClicked(_ sender: Any) {
        let vc = OnboardingViewController.initFromXIB()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
}

