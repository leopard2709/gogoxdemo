//
//  UIViewController+Extension.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 15/4/2021.
//

import UIKit

extension UIViewController {

    public class func initFromXIB() -> Self {
        func helper<T>() -> T where T: UIViewController {
            return T(nibName: String(describing: self), bundle: nil)
        }

        return helper()
    }
}


