//
//  OnboardingModel.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import Foundation

struct OnboardingModel {

    var origin: GGXPlace?
    var destination: GGXPlace?
}
