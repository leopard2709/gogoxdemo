//
//  GGXPlace.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import Foundation

struct GGXPlace {
    let name: String
    let gPlaceId: String
}
