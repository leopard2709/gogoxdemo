//
//  EnterRouteModel.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import Foundation

struct EnterRouteModel {

    var searchResult: [GGXPlace]?

    var origin: GGXPlace?
    var destination: GGXPlace?
}
