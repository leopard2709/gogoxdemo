//
//  KeyboardHelper.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import UIKit

protocol KeyboardHelperDelegate {
    func keyboardChangedFrame(isAppear: Bool, keyboardHeight: CGFloat, duration: Double)
}

class KeyboardHelper: NSObject {

    static func addListener(vc: UIViewController) {
        NotificationCenter.default.addObserver(vc, selector: #selector(vc.keyboardNotification(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(vc, selector: #selector(vc.keyboardNotification(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

extension UIViewController {

    @objc func keyboardNotification(_ notification: NSNotification) {
        guard let keyboardInfo = notification.userInfo as NSDictionary? else { return }
        let keyboardHeight = (keyboardInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size.height
        let duration = (keyboardInfo.value(forKey: UIResponder.keyboardAnimationDurationUserInfoKey) as AnyObject).doubleValue
        let vc = (self as? KeyboardHelperDelegate)

        vc?.keyboardChangedFrame(isAppear: notification.name != UIResponder.keyboardWillHideNotification,
                                 keyboardHeight: keyboardHeight,
                                 duration: duration ?? 0)
    }
}
