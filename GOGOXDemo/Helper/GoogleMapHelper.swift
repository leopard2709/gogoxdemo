//
//  GoogleMapHelper.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import Foundation
import GoogleMaps
import GooglePlaces

struct GoogleMapHelper {

    static let mapApiKey = "AIzaSyD7IACqsWrjr46Zw6vz49oRyYa60U5OkyY"
    static let placesApiKey = "AIzaSyBMzumkTONdlqq9OcW25U_739BV_Mytltw"

    static func registerGMSServices() {
        GMSServices.provideAPIKey(mapApiKey)
    }

    static func registerPlacesClient() {
        GMSPlacesClient.provideAPIKey(placesApiKey)
    }
}
