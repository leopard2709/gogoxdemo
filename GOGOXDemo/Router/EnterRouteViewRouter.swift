//
//  EnterRouteViewRouter.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import Foundation

protocol EnterRouteViewRouteLogic {
    var vc: EnterRouteViewController? { get set }

    func dismiss()
}

class EnterRouteViewRouter: EnterRouteViewRouteLogic {

    weak var vc: EnterRouteViewController?

    func dismiss() {
        vc?.dismiss(animated: true, completion: nil)
    }
}
