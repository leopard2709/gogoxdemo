//
//  OnboardingViewRouter.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import UIKit

protocol OnboardingViewRouteLogic {
    var vc: OnboardingViewController? { get set }

    func routeToEnterRoute()
}

class OnboardingViewRouter: OnboardingViewRouteLogic {

    weak var vc: OnboardingViewController?

    func routeToEnterRoute() {
        let routeVC = EnterRouteViewController.initFromXIB()
        routeVC.modalPresentationStyle = .fullScreen
        routeVC.delegate = vc
        routeVC.viewModel.model.origin = vc?.viewModel.model.origin
        routeVC.viewModel.model.destination = vc?.viewModel.model.destination

        vc?.present(routeVC, animated: true, completion: nil)
    }
}
