//
//  OnboardingViewController.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 15/4/2021.
//

import UIKit

protocol OnboardingViewDisplayLogic: class {
    func displayRoutePlaceUpdate()
}

class OnboardingViewController: UIViewController, OnboardingViewDisplayLogic {

    @IBOutlet weak var routeBarView: RouteBarView!

    public var router: OnboardingViewRouteLogic = OnboardingViewRouter()
    public var viewModel: OnboardingViewLogic = OnboardingViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        initUI()
    }

    private func setup() {
        router.vc = self
        viewModel.vc = self
    }

    private func initUI() {
        title = "Gogovan"

        routeBarView.isDisplayMode = true
        routeBarView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(routeBarClicked)))
    }

    @objc public func routeBarClicked() {
        router.routeToEnterRoute()
    }

    func displayRoutePlaceUpdate() {
        routeBarView.originTextField.text = viewModel.model.origin?.name
        routeBarView.destinationTextField.text = viewModel.model.destination?.name
    }
}

extension OnboardingViewController: EnterRouteViewControllerDelegate {

    func updateRoutePlace(origin: GGXPlace?, destination: GGXPlace?) {
        viewModel.toUpdateRoutePlace(origin: origin, destination: destination)
    }
}

