//
//  EnterRouteViewController.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import UIKit

protocol EnterRouteViewDisplayLogic: class {
    func displayReloadResultList()
}

protocol EnterRouteViewControllerDelegate: class {
    func updateRoutePlace(origin: GGXPlace?, destination: GGXPlace?)
}

class EnterRouteViewController: UIViewController {

    static let cellIdentifier = "cell"

    @IBOutlet weak var routeBarView: RouteBarView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!

    public weak var delegate: EnterRouteViewControllerDelegate?

    public var router: EnterRouteViewRouteLogic = EnterRouteViewRouter()
    public var viewModel: EnterRouteViewLogic = EnterRouteViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        initUI()
        initTableView()
    }

    private func initUI() {
        routeBarView.originTextField.text = viewModel.model.origin?.name
        routeBarView.destinationTextField.text = viewModel.model.destination?.name

        if routeBarView.originTextField.text?.isEmpty ?? true {
            routeBarView.originTextField.becomeFirstResponder()
        } else if routeBarView.destinationTextField.text?.isEmpty ?? true {
            routeBarView.destinationTextField.becomeFirstResponder()
        } else {
            routeBarView.originTextField.becomeFirstResponder()
        }
    }

    private func setup() {
        KeyboardHelper.addListener(vc: self)

        routeBarView.delegate = self

        router.vc = self
        viewModel.vc = self
    }

    private func initTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: EnterRouteViewController.cellIdentifier)

        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func backButtonClicked(_ sender: Any) {
        delegate?.updateRoutePlace(origin: viewModel.model.origin, destination: viewModel.model.destination)
        router.dismiss()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension EnterRouteViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.model.searchResult?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EnterRouteViewController.cellIdentifier, for: indexPath)

        if let list = viewModel.model.searchResult {
            cell.textLabel?.text = list[indexPath.row].name
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let list = viewModel.model.searchResult else { return }

        switch viewModel.currentInput {
        case .origin:
            viewModel.model.origin = list[indexPath.row]
            routeBarView.originTextField.text = list[indexPath.row].name
        case .destination:
            viewModel.model.destination = list[indexPath.row]
            routeBarView.destinationTextField.text = list[indexPath.row].name
        default:
            break
        }
    }
}

extension EnterRouteViewController: RouteBarViewDelegate {

    func routeChanged(place: String, input: RouteBarView.RouteInput) {
        viewModel.currentInput = input
        viewModel.toGetAutocomplete(keyword: place)
    }
}

extension EnterRouteViewController: EnterRouteViewDisplayLogic {

    func displayReloadResultList() {
        tableView.reloadData()
    }
}

extension EnterRouteViewController: KeyboardHelperDelegate {

    func keyboardChangedFrame(isAppear: Bool, keyboardHeight: CGFloat, duration: Double) {
        UIView.animate(withDuration: duration) {
            self.tableViewBottomConstraint.constant = isAppear ? keyboardHeight : 0
            self.view.layoutIfNeeded()
        }
    }
}
