//
//  OnboardingViewModel.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import Foundation

protocol OnboardingViewLogic {
    var model: OnboardingModel { get }
    var vc: OnboardingViewDisplayLogic? { get set }

    func toUpdateRoutePlace(origin: GGXPlace?, destination: GGXPlace?)
}

class OnboardingViewModel: OnboardingViewLogic {

    var model: OnboardingModel = OnboardingModel()

    weak var vc: OnboardingViewDisplayLogic?

    func toUpdateRoutePlace(origin: GGXPlace?, destination: GGXPlace?) {
        model.origin = origin
        model.destination = destination

        vc?.displayRoutePlaceUpdate()
    }
}
