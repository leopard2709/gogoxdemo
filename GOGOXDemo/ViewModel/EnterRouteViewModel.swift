//
//  EnterRouteViewModel.swift
//  GOGOXDemo
//
//  Created by Alex Ng on 16/4/2021.
//

import Foundation
import GooglePlaces

protocol EnterRouteViewLogic {
    var model: EnterRouteModel { get set }
    var service: GooglePlaceService { get set }
    var vc: EnterRouteViewDisplayLogic? { get set }
    var currentInput: RouteBarView.RouteInput? { get set }

    func toGetAutocomplete(keyword: String)
}

class EnterRouteViewModel: EnterRouteViewLogic {

    var model: EnterRouteModel = EnterRouteModel()
    var service: GooglePlaceService = GooglePlaceService()

    var currentInput: RouteBarView.RouteInput?

    weak var vc: EnterRouteViewDisplayLogic?

    func toGetAutocomplete(keyword: String) {
        service.getAutocomplete(keyword: keyword) { (result) in
            switch result {
            case .success(let list):
                self.model.searchResult = list
                self.vc?.displayReloadResultList()
            case .failure(_):
                break
            }
        }
    }
}
